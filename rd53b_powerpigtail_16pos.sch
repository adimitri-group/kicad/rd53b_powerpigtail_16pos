EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "16 pin power pigtail for rd53b_quad hybrid design"
Date "2020-11-23"
Rev "1.1"
Comp "LBNL"
Comment1 "Aleksandra Dimitrievska"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CLM-108-02-L-D-A:CLM-108-02-L-D-A J1
U 1 1 5F90A33B
P 2700 2700
F 0 "J1" H 2700 3367 50  0000 C CNN
F 1 "CLM-108-02-L-D-A" H 2700 3276 50  0000 C CNN
F 2 "Library:SAMTEC_CLM-108-02-L-D-A" H 2700 2700 50  0001 L BNN
F 3 "Samtec" H 2700 2700 50  0001 L BNN
F 4 "R" H 2700 2700 50  0001 L BNN "Field4"
F 5 "Manufacturer Recommendations" H 2700 2700 50  0001 L BNN "Field5"
	1    2700 2700
	1    0    0    -1  
$EndComp
$Comp
L CLM-108-02-L-D-A:CLM-108-02-L-D-A J2
U 1 1 5F90B077
P 5600 2600
F 0 "J2" H 5600 3267 50  0000 C CNN
F 1 "CLM-108-02-L-D-A" H 5600 3176 50  0000 C CNN
F 2 "Library:SAMTEC_CLM-108-02-L-D-A" H 5600 2600 50  0001 L BNN
F 3 "Samtec" H 5600 2600 50  0001 L BNN
F 4 "R" H 5600 2600 50  0001 L BNN "Field4"
F 5 "Manufacturer Recommendations" H 5600 2600 50  0001 L BNN "Field5"
	1    5600 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	2200 2400 2100 2400
Wire Wire Line
	1850 2400 1850 1650
Wire Wire Line
	1850 1650 6500 1650
Wire Wire Line
	2200 2300 2200 2400
Connection ~ 2200 2400
Connection ~ 3200 2400
Wire Wire Line
	3200 2400 3200 2300
Wire Wire Line
	3200 2500 3200 2400
Wire Wire Line
	3200 2300 3200 1950
Wire Wire Line
	3200 1950 2100 1950
Wire Wire Line
	2100 1950 2100 2400
Connection ~ 3200 2300
Connection ~ 2100 2400
Wire Wire Line
	2100 2400 1850 2400
Wire Wire Line
	6100 2400 6100 2300
Wire Wire Line
	5100 2400 5100 2300
Wire Wire Line
	5100 2300 5100 2100
Wire Wire Line
	5100 2100 6200 2100
Connection ~ 5100 2300
Wire Wire Line
	1850 3500 6500 3500
Connection ~ 6100 2900
Wire Wire Line
	6100 2900 6100 3000
Wire Wire Line
	5100 2900 5100 3000
Wire Wire Line
	5100 3000 5100 3350
Wire Wire Line
	5100 3350 6200 3350
Connection ~ 5100 3000
Wire Wire Line
	2200 2900 2200 3000
Wire Wire Line
	3200 2800 3200 2900
Connection ~ 3200 2900
Wire Wire Line
	3200 2900 3200 3000
Wire Wire Line
	3200 3000 3200 3250
Wire Wire Line
	3200 3250 2100 3250
Connection ~ 3200 3000
Text Label 3600 2700 2    50   ~ 0
LP_Enable
Wire Wire Line
	2200 2500 1750 2500
Text Label 1750 2500 0    50   ~ 0
NTC
Text Label 1750 2600 0    50   ~ 0
NTC_RET
Text Label 1750 2700 0    50   ~ 0
HV
Text Label 3750 1650 0    50   ~ 0
GND
Text Label 3750 3500 0    50   ~ 0
VIN
Wire Wire Line
	1850 3500 1850 2900
Wire Wire Line
	1850 2900 2100 2900
Connection ~ 2200 2900
Wire Wire Line
	2100 3250 2100 2900
Connection ~ 2100 2900
Wire Wire Line
	2100 2900 2200 2900
Wire Wire Line
	3200 2500 3200 2600
Connection ~ 3200 2500
Wire Wire Line
	2200 2800 2200 2900
Wire Wire Line
	5100 2400 5100 2500
Connection ~ 5100 2400
Wire Wire Line
	6700 1450 1650 1450
Wire Wire Line
	1650 1450 1650 2600
Wire Wire Line
	1650 2600 2200 2600
Wire Wire Line
	6800 1350 1550 1350
Wire Wire Line
	1550 1350 1550 2700
Wire Wire Line
	1550 2700 2200 2700
Wire Wire Line
	6500 3500 6500 2900
Connection ~ 5100 2900
Wire Wire Line
	5100 2800 5100 2900
Wire Wire Line
	6100 2900 6200 2900
Wire Wire Line
	6200 3350 6200 2900
Connection ~ 6200 2900
Wire Wire Line
	6200 2900 6500 2900
Wire Wire Line
	3200 2700 4450 2700
Wire Wire Line
	5100 2700 5100 2800
Connection ~ 5100 2800
Wire Wire Line
	5100 2600 4450 2600
Wire Wire Line
	4450 2600 4450 2700
Wire Wire Line
	6100 2500 6200 2500
Wire Wire Line
	6500 1650 6500 2500
Wire Wire Line
	6100 2400 6100 2500
Connection ~ 6100 2400
Connection ~ 6100 2500
Wire Wire Line
	6200 2100 6200 2500
Connection ~ 6200 2500
Wire Wire Line
	6200 2500 6500 2500
Wire Wire Line
	6700 1450 6700 2700
Wire Wire Line
	6100 2700 6700 2700
Wire Wire Line
	6100 2800 6600 2800
Wire Wire Line
	1750 2500 1750 1550
Wire Wire Line
	1750 1550 6600 1550
Wire Wire Line
	6800 1350 6800 2600
Wire Wire Line
	6600 1550 6600 2800
Wire Wire Line
	6100 2600 6800 2600
$EndSCHEMATC
